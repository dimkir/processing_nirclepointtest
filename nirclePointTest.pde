final int THOUSAND = 1000;
final int C_POINT_COUNT = 100 *THOUSAND;
final color C_PIXEL_COLOR = #FFFFFF;





RandomPoints[] pointSets = new RandomPoints[2];
int curPointsetIndex = 0;

void setup(){
   size(800, 600);
   textFont(createFont("Arial",22));
   pointSets[0] = new RandomPoints(C_POINT_COUNT);
   pointSets[1] = new RandomPoints(C_POINT_COUNT);
}




void draw(){
    background(0);
    // we choose different point sets 
    RandomPoints pointsToDraw = pointSets[curPointsetIndex];
    curPointsetIndex++;
    curPointsetIndex %= pointSets.length;
    
    if ( !mousePressed ){
         echoFrameRate("Drawing points with classic method: point().  ");
        drawAllPointsClassic(pointsToDraw);  // this will be slow
    }
    else{
        echoFrameRate("Drawing points with pixel array. ");
        drawAllPointsViaPixelArray(pointsToDraw);  // this will be fast
    }
    
}


void echoFrameRate(String s){
   fill(#FF0000);
   text(s + "FPS: " + frameRate , 10, 20);
}



/**
* Just loops through all the points and plots them with point()
*/
void drawAllPointsClassic(RandomPoints rp){
    noFill();
    stroke(C_PIXEL_COLOR);
    for(int i = 0;  i < rp.pointsX.length ; i++){
       point(rp.pointsX[i], rp.pointsY[i]);
    } 
}


/**
*
*/
void drawAllPointsViaPixelArray(RandomPoints rp){
    loadPixels();
    for(int i = 0;  i < rp.pointsX.length ; i++){
       nirclePoint(rp.pointsX[i], rp.pointsY[i], width, pixels, C_PIXEL_COLOR); // we need those extra parameters
    } 
    updatePixels();
}

  /**
  * Plots  
  */
  void nirclePoint(float xx, float yy, int mWidth, color[] pixelz , color pixelColor){
         int ixx = (int) xx;
         int iyy = (int) yy;
         int pixelIndex = iyy * mWidth + ixx;
         if ( pixelIndex < 0  || pixelIndex >= pixelz.length ){
              // we just don't plot pixels outside of range
              // println("Pixel index is out of bounds: " + pixelIndex + " pixel array size: " + mPixelz.length);
         }
         else{
           pixelz[pixelIndex] = pixelColor;
         } 

  } // ?
  
  
  
  
/**
* This is just array of point X & Y coordinates wrapped into a class. (For convenince)
*/
class RandomPoints
{  
  
  float[] pointsX;
  float[] pointsY;

  
   RandomPoints(int qty){
       initPoints(qty);     
   }
   
   void initPoints(int pointCount){
   pointsX = new float[pointCount];
   pointsY = new float[pointCount];
   for(int i = 0 ; i < pointCount ; i++){
      pointsX[i] = random(width);
      pointsY[i] = random(30, height);
   }
   
}
   
   
}
